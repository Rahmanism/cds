unit Unit1;

interface

uses
  Windows, Messages, Classes, SysUtils, Graphics, Controls, StdCtrls, Forms,
  Dialogs, DBCtrls, DB, Grids, DBGrids, DBTables, Mask, ExtCtrls, ADODB,
  XPMan;

type
  TForm1 = class(TForm)
    ScrollBox: TScrollBox;
    Label1: TLabel;
    EditCID: TDBEdit;
    Label2: TLabel;
    EditGNo: TDBEdit;
    Label3: TLabel;
    EditGroup: TDBEdit;
    Label4: TLabel;
    EditName: TDBEdit;
    Label5: TLabel;
    EditBDate: TDBEdit;
    Label6: TLabel;
    EditPrice: TDBEdit;
    Label8: TLabel;
    DBNavigator: TDBNavigator;
    Panel1: TPanel;
    DataSource1: TDataSource;
    Panel2: TPanel;
    Panel3: TPanel;
    DBGrid1: TDBGrid;
    DBMemo1: TDBMemo;
    Label10: TLabel;
    EditProducer: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    LabeledEdit1: TLabeledEdit;
    LabeledEdit2: TLabeledEdit;
    Query1: TQuery;
    Label7: TLabel;
    Edit1: TEdit;
    Table1: TTable;
    DBCheckBox3: TDBCheckBox;
    XPManifest1: TXPManifest;
    DBCheckBox4: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure LabeledEdit2Change(Sender: TObject);
    procedure LabeledEdit1Change(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure DBGrid1TitleClick(Column: TColumn);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.FormCreate(Sender: TObject);
begin
  Table1.Open;
end;

procedure TForm1.LabeledEdit2Change(Sender: TObject);
begin
  if LabeledEdit2.Text='' then
    exit;
  Query1.Active := False;
  Query1.SQL.Clear;
  Query1.SQL.Add('select * from "..\pdata\CDs.db" c where c.cid='
    + LabeledEdit2.Text);
  Query1.Active := True;
  Table1.Locate('cid', Query1['cid'],[]);
end;

procedure TForm1.LabeledEdit1Change(Sender: TObject);
begin
  if LabeledEdit1.Text='' then
    exit;
  Query1.Active := False;
  Query1.SQL.Clear;
  Query1.SQL.Add('select * from "..\pdata\CDs.db" c where lower(c.title) like '
    + QuotedStr('%' + LowerCase(LabeledEdit1.Text) + '%'));
  Query1.Active := True;
  Table1.Locate('cid', Query1['cid'],[]);
end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
  if Edit1.Text='' then
    exit;
  Query1.Active := False;
  Query1.SQL.Clear;
  Query1.SQL.Add('select * from "..\pdata\CDs.db" c where c.title like '
    + QuotedStr('%' + Edit1.Text + '%'));
  Query1.Active := True;
  Table1.Locate('cid', Query1['cid'],[]);
end;

procedure TForm1.DBGrid1TitleClick(Column: TColumn);
begin
//  Table1.Sort := Column.FieldName + ', CID';
end;

end.
